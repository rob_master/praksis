import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns
#sns.set()

x = np.linspace(1,2000, 10000)
h = 6.626*10**(-34) # J*s
k_B = 1.381*10**(-23) # J/K
c = 299792458 # m/s

def planck(x, T):
    x = x*10**(-9)
    return ((2*h*c**2)/x**5)*(1/(np.exp((h*c)/(x*k_B*T)) - 1))

plt.plot(x, planck(x, 5800), 'r', label = "5800 K")
plt.legend()
plt.grid(linestyle = '-')
plt.xlabel(r'$\lambda / nm$')
plt.ylabel(r'$\frac{W}{m^2 \cdot nm}$')
plt.savefig('5800K.png')
